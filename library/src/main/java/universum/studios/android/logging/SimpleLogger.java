/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.logging;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A simple implementation of {@link Logger} which delegates all its calls to {@link Log} class.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class SimpleLogger implements Logger {

	/*
	 * Constants ===================================================================================
	 */

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Logging level for this logger.
	 */
	@Level private int logLevel;

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of SimpleLogger with the specified log level.
	 *
	 * @param level The initial logging level for the logger.
	 * @see #setLogLevel(int)
	 * @see #getLogLevel()
	 */
	public SimpleLogger(@Level final int level) {
		this.logLevel = level;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override public void setLogLevel(@Level final int level) {
		this.logLevel = level;
	}

	/**
	 */
	@Override @Level public int getLogLevel() {
		return logLevel;
	}

	/**
	 */
	@Override public boolean isLoggable(@NonNull final String tag, final int level) {
		return logLevel <= level;
	}

	/**
	 */
	@Override public void d(@NonNull final String tag, @NonNull final String message) {
		if (isLoggable(tag, Log.DEBUG)) Log.d(tag, message);
	}

	/**
	 */
	@Override public void d(@NonNull final String tag, @NonNull final String message, @Nullable final Throwable throwable) {
		if (isLoggable(tag, Log.DEBUG)) Log.d(tag, message, throwable);
	}

	/**
	 */
	@Override public void v(@NonNull final String tag, @NonNull final String message) {
		if (isLoggable(tag, Log.VERBOSE)) Log.v(tag, message);
	}

	/**
	 */
	@Override public void v(@NonNull final String tag, @NonNull final String message, @Nullable final Throwable throwable) {
		if (isLoggable(tag, Log.VERBOSE)) Log.v(tag, message, throwable);
	}

	/**
	 */
	@Override public void i(@NonNull final String tag, @NonNull final String message) {
		if (isLoggable(tag, Log.INFO)) Log.i(tag, message);
	}

	/**
	 */
	@Override public void i(@NonNull final String tag, @NonNull final String message, @Nullable final Throwable throwable) {
		if (isLoggable(tag, Log.INFO)) Log.i(tag, message, throwable);
	}

	/**
	 */
	@Override public void w(@NonNull final String tag, @NonNull final String message) {
		if (isLoggable(tag, Log.WARN)) Log.w(tag, message);
	}

	/**
	 */
	@Override public void w(@NonNull final String tag, @Nullable final Throwable throwable) {
		if (isLoggable(tag, Log.WARN)) Log.w(tag, throwable);
	}

	/**
	 */
	@Override public void w(@NonNull final String tag, @NonNull final String message, @Nullable final Throwable throwable) {
		if (isLoggable(tag, Log.WARN)) Log.w(tag, message, throwable);
	}

	/**
	 */
	@Override public void e(@NonNull final String tag, @NonNull final String message) {
		if (isLoggable(tag, Log.ERROR)) Log.e(tag, message);
	}

	/**
	 */
	@Override public void e(@NonNull final String tag, @NonNull final String message, @Nullable final Throwable throwable) {
		if (isLoggable(tag, Log.ERROR)) Log.e(tag, message, throwable);
	}

	/**
	 */
	@Override public void wtf(@NonNull final String tag, @NonNull final String message) {
		Log.wtf(tag, message);
	}

	/**
	 */
	@Override public void wtf(@NonNull final String tag, @Nullable final Throwable throwable) {
		Log.wtf(tag, throwable);
	}

	/**
	 */
	@Override public void wtf(@NonNull final String tag, @NonNull final String message, @Nullable final Throwable throwable) {
		Log.wtf(tag, message, throwable);
	}

	/**
	 */
	@Override public void log(@Level final int level, @NonNull final String tag, @NonNull final String message) {
		if (isLoggable(tag, level)) forceLog(level, tag, message);
	}

	/**
	 */
	@Override public void forceLog(@Level final int level, @NonNull final String tag, @NonNull final String message) {
		Log.println(level, tag, message);
	}

	/**
	 */
	@Override @Nullable public String getStackTraceString(@Nullable final Throwable throwable) {
		return Log.getStackTraceString(throwable);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}