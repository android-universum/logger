/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.logging;

import android.util.AndroidRuntimeException;
import android.util.Log;

import org.junit.Test;

import universum.studios.android.testing.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class SimpleLoggerTest extends AndroidTestCase {

	@SuppressWarnings("unused") private static final String TAG = "SimpleLoggerTest";

	private static final int[] sLogLevels = {
			Log.VERBOSE,
			Log.DEBUG,
			Log.INFO,
			Log.WARN,
			Log.ERROR,
			Log.ASSERT
	};

	@Test public void testInstantiation() {
		// Act:
		final Logger logger = new SimpleLogger(Log.VERBOSE);

		// Assert:
		assertThat(logger.getLogLevel(), is(Log.VERBOSE));
	}

	@Test public void testLogLevel() {
		// Arrange:
		final Logger logger = new SimpleLogger(Log.VERBOSE);

		// Act:
		logger.setLogLevel(Log.ERROR);
		assertThat(logger.getLogLevel(), is(Log.ERROR));
	}

	@Test public void testIsLoggable() {
		// Arrange:
		final Logger logger = new SimpleLogger(Log.VERBOSE);

		// Act:

		// 1)
		logger.setLogLevel(Log.ASSERT);
		assertThat(logger.isLoggable(TAG, Log.VERBOSE), is(false));
		assertThat(logger.isLoggable(TAG, Log.DEBUG), is(false));
		assertThat(logger.isLoggable(TAG, Log.INFO), is(false));
		assertThat(logger.isLoggable(TAG, Log.WARN), is(false));
		assertThat(logger.isLoggable(TAG, Log.ERROR), is(false));
		assertThat(logger.isLoggable(TAG, Log.ASSERT), is(true));

		// 2)
		logger.setLogLevel(Log.ERROR);
		assertThat(logger.isLoggable(TAG, Log.VERBOSE), is(false));
		assertThat(logger.isLoggable(TAG, Log.DEBUG), is(false));
		assertThat(logger.isLoggable(TAG, Log.INFO), is(false));
		assertThat(logger.isLoggable(TAG, Log.WARN), is(false));
		assertThat(logger.isLoggable(TAG, Log.ERROR), is(true));
		assertThat(logger.isLoggable(TAG, Log.ASSERT), is(true));

		// 3)
		logger.setLogLevel(Log.WARN);
		assertThat(logger.isLoggable(TAG, Log.VERBOSE), is(false));
		assertThat(logger.isLoggable(TAG, Log.DEBUG), is(false));
		assertThat(logger.isLoggable(TAG, Log.INFO), is(false));
		assertThat(logger.isLoggable(TAG, Log.WARN), is(true));
		assertThat(logger.isLoggable(TAG, Log.ERROR), is(true));
		assertThat(logger.isLoggable(TAG, Log.ASSERT), is(true));

		// 4)
		logger.setLogLevel(Log.INFO);
		assertThat(logger.isLoggable(TAG, Log.VERBOSE), is(false));
		assertThat(logger.isLoggable(TAG, Log.DEBUG), is(false));
		assertThat(logger.isLoggable(TAG, Log.INFO), is(true));
		assertThat(logger.isLoggable(TAG, Log.WARN), is(true));
		assertThat(logger.isLoggable(TAG, Log.ERROR), is(true));
		assertThat(logger.isLoggable(TAG, Log.ASSERT), is(true));

		// 5)
		logger.setLogLevel(Log.DEBUG);
		assertThat(logger.isLoggable(TAG, Log.VERBOSE), is(false));
		assertThat(logger.isLoggable(TAG, Log.DEBUG), is(true));
		assertThat(logger.isLoggable(TAG, Log.INFO), is(true));
		assertThat(logger.isLoggable(TAG, Log.WARN), is(true));
		assertThat(logger.isLoggable(TAG, Log.ERROR), is(true));
		assertThat(logger.isLoggable(TAG, Log.ASSERT), is(true));

		// 6)
		logger.setLogLevel(Log.VERBOSE);
		assertThat(logger.isLoggable(TAG, Log.VERBOSE), is(true));
		assertThat(logger.isLoggable(TAG, Log.DEBUG), is(true));
		assertThat(logger.isLoggable(TAG, Log.INFO), is(true));
		assertThat(logger.isLoggable(TAG, Log.WARN), is(true));
		assertThat(logger.isLoggable(TAG, Log.ERROR), is(true));
		assertThat(logger.isLoggable(TAG, Log.ASSERT), is(true));
	}

	@Test public void testVerbose() {
		// Arrange:
		final Logger logger = new SimpleLogger(Log.VERBOSE);

		// Act:

		// With enabled level.
		logger.setLogLevel(Log.VERBOSE);
		logger.v(TAG, "");
		logger.v(TAG, "", new UnsupportedOperationException());

		// With disabled level.
		logger.setLogLevel(Log.ASSERT);
		logger.v(TAG, "");
		logger.v(TAG, "", new UnsupportedOperationException());
	}

	@Test public void testDebug() {
		// Arrange:
		final Logger logger = new SimpleLogger(Log.VERBOSE);

		// Act:

		// With enabled level.
		logger.setLogLevel(Log.DEBUG);
		logger.d(TAG, "");
		logger.d(TAG, "", new UnsupportedOperationException());

		// With disabled level.
		logger.setLogLevel(Log.ASSERT);
		logger.d(TAG, "");
		logger.d(TAG, "", new UnsupportedOperationException());
	}

	@Test public void testInfo() {
		// Arrange:
		final Logger logger = new SimpleLogger(Log.VERBOSE);

		// Act:

		// With enabled level.
		logger.setLogLevel(Log.INFO);
		logger.i(TAG, "");
		logger.i(TAG, "", new IllegalArgumentException());

		// With disabled level.
		logger.setLogLevel(Log.ASSERT);
		logger.i(TAG, "");
		logger.i(TAG, "", new IllegalArgumentException());
	}

	@Test public void testWarn() {
		// Arrange:
		final Logger logger = new SimpleLogger(Log.VERBOSE);

		// Act:

		// With enabled level.
		logger.setLogLevel(Log.WARN);
		logger.w(TAG, "");
		logger.w(TAG, "", new IllegalArgumentException());
		logger.w(TAG, new IllegalArgumentException());

		// With disabled level.
		logger.setLogLevel(Log.ASSERT);
		logger.w(TAG, "");
		logger.w(TAG, "", new IllegalArgumentException());
		logger.w(TAG, new IllegalArgumentException());
	}

	@Test public void testError() {
		// Arrange:
		final Logger logger = new SimpleLogger(Log.VERBOSE);

		// Act:

		// With enabled level.
		logger.setLogLevel(Log.ERROR);
		logger.e(TAG, "");
		logger.e(TAG, "", new AndroidRuntimeException());

		// With disabled level.
		logger.setLogLevel(Log.ASSERT);
		logger.e(TAG, "");
		logger.e(TAG, "", new AndroidRuntimeException());
	}

	@Test public void testWtf() {
		// Arrange:
		final Logger logger = new SimpleLogger(Log.VERBOSE);

		// Act:
		logger.wtf(TAG, "");
		logger.wtf(TAG, "", new AndroidRuntimeException());
		logger.wtf(TAG, new AndroidRuntimeException());
	}

	@Test public void testLog() {
		// Arrange:
		final Logger logger = new SimpleLogger(Log.VERBOSE);

		// Act:
		for (final int logLevel : sLogLevels) {
			// With enabled level.
			logger.setLogLevel(logLevel);
			logger.log(logLevel, TAG, "");
			// With disabled level.
			logger.setLogLevel(Log.ASSERT);
			logger.log(logLevel, TAG, "");
		}
	}

	@Test public void testForceLog() {
		// Arrange:
		final Logger logger = new SimpleLogger(Log.VERBOSE);

		// Act:
		logger.forceLog(Log.VERBOSE, TAG, "");
		logger.forceLog(Log.DEBUG, TAG, "");
		logger.forceLog(Log.INFO, TAG, "");
		logger.forceLog(Log.WARN, TAG, "");
		logger.forceLog(Log.ERROR, TAG, "");
		logger.forceLog(Log.ASSERT, TAG, "");
	}

	@Test public void testGetStackTraceString() {
		// Arrange:
		final Logger logger = new SimpleLogger(Log.VERBOSE);

		// Act:
		final String stackTraceString = logger.getStackTraceString(new IllegalArgumentException("Invalid argument."));

		// Assert:
		assertThat(stackTraceString, startsWith("java.lang.IllegalArgumentException: Invalid argument."));
	}
}